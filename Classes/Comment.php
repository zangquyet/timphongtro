<?php 

class Comment
{
	private $table= "comments";

	public function tongbinhluan()
	{
		$link=Db::getInstance();
		$sql="SELECT COUNT(id) AS count FROM {$this->table}";
		$result=mysqli_query($link,$sql);
		$row=mysqli_fetch_assoc($result);
		$count=$row['count'];
		Db::close($link);
		return $count;
	}

	public function all()
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table}";
		$result=mysqli_query($link,$sql);
		Db::close($link);
		$list=array();
		while ($row=mysqli_fetch_assoc($result)) {
			$list[]=$row;
		}
		return $list;
	}
	public function getComment($id_post)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE p_id={$id_post}";
		$result=mysqli_query($link,$sql);
		$list=array();
		while ($row=mysqli_fetch_assoc($result)) {
			$list[]=$row;
		}
		return $list;
		Db::close($link);
	}

	public function issetPost($id)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE id={$id}";
		$result=mysqli_query($link,$sql);
		return mysqli_num_rows($result)==1?true:false;
	}
	public function delete($id)
	{
		$sql="DELETE FROM {$this->table} WHERE id={$id}";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}

	public function duyet($id)
	{
		$sql="UPDATE {$this->table} SET trangthai=1 WHERE id={$id}";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}
		else{
			Db::close($link);
			return false;
		}
	}
	public function xoaduyet($id)
	{
		$sql="UPDATE {this->table} SET trangthai=2 WHERE id={$id}";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}
		else{
			Db::close($link);
			return false;
		}
	}
	public function add($array)
	{
		$link=Db::getInstance();
		$field=[];
		$val=[];
		foreach ($array as $key => $value) {
			$field[]=$key;
			$val[]="'".$value."'";
		}
		$values=implode(',', $val);
		$fie=implode(',', $field);
		$sql="INSERT INTO {$this->table} ({$fie}) VALUES ($values)";
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else{
			echo mysqli_error($link);
			Db::close($link);
			return false;
		}	
	}

	
}

?>

