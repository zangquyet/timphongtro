<?php 
/**
* 
*/
class Main
{
	private static function loadController($controller)
	{	
		$folderController='Controller/';
		$file= $folderController.$controller."Controller.php";
		if (file_exists($file)) {
			include $folderController.$controller."Controller.php";
		}else {
			return false;
		}
		return true;
		//include "Controller/HomeController.php";
	}
	public function init()
	{
		if (isset($_GET['controller'])) {
			$controller=$_GET['controller'];
		}else {
			$controller='post';
		}
		if (isset($_GET['action'])) {
			$action=$_GET['action'];
		}else {
			$action='index';
		}
		//echo $controller.$action;
		/*include 'Controller/HomeController.php';
		$objController=new HomeController();
		$objController->index();*/
		if (Main::loadController($controller)) {
			$ctrl=$controller."Controller";
			$objController=new $ctrl;
			if (method_exists($objController, $action)) {
				$objController->{$action}();
			}else{
				include 'Views/pages/404.php';
			}
		}else{
			include 'Views/pages/404.php';
			
		}
		/*$ctrl=$controller."Controller()";
		$objController=new $ctrl;
		$objController->{$action}();*/
	}
}

?>