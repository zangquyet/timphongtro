<?php 
/**
 * summary
 */
class Helper
{
    /**
     * summary
     */
    // cậu clone thiếu à
    public static $siteurl="http://localhost/timphongtro/";
    public static $num_per_page=8;
    public static function isAdmin()
    {
      return $_SESSION['level']==1;
    }
   
    public static function xoafile($path)
    {
      return unlink('../'.$path);
    }
    public static function getHeader()
    {
    	include 'Views/partial/header.php';
    }

    public static function getFooter()
    {
    	include 'Views/partial/footer.php';
    }
    public function getPagination($url,$current,$total)
    {
      if ($total==1) {
        return;
      }
    	if ($current==1) {
    		$return="<ul class=\"pagination\">"
            ."<li class='active'><a href=\"javascript:void(0)\">Trang đầu</a></li>"
            ."<li><a href=\"javascript:void(0)\">Trang trước</a></li>"
            ."<li><a href=\"".$url.($current+1)."\">Trang sau</a></li>"
            ."<li><a href=\"".$url.$total."\">Trang Cuối</a></li>"
            ."</ul>";
        }elseif ($current==$total) {
          $return="<ul class=\"pagination\">"
          ."<li><a href=\"".$url."1\">Trang đầu</a></li>"
          ."<li><a href=\"".$url.($current-1)."\">Trang trước</a></li>"
          ."<li><a href=\"javascript:void(0)\">Trang sau</a></li>"
          ."<li class='active'><a href=\"javascript:void(0)\">Trang Cuối</a></li>"
          ."</ul>";
      }else {
          $return="<ul class=\"pagination\">"
          ."<li><a href=\"".$url."1\">Trang đầu</a></li>"
          ."<li><a href=\"".$url.($current-1)."\">Trang trước</a></li>"
          ."<li><a href=\"".$url.($current+1)."\">Trang sau</a></li>"
          ."<li><a href=\"".$url.$total."\">Trang Cuối</a></li>"
          ."</ul>";
      }

      return $return;
  }

  public static function redirect($localtion)
  {
   header("Location:{$localtion}");
}
public static function getImage($forlder)
{
   $forlder="uploads/".$forlder;	
   $return= scandir($forlder);
   unset($return[0]);
   unset($return[1]);
   return $return;
}

//hàm cắt bớt sâu
public function getshorterstring($string,$length)
{
   return mb_substr($string,0,$length,'UTF-8');
}
public function getKhuVuc($id)
{
   $url='https://thongtindoanhnghiep.co/api/district/'.$id;
   $khuvuc=file_get_contents($url);
   $kv=json_decode($khuvuc);
   return $kv->Title." , ".$kv->TinhThanhTitle;
}
// ví dụ
public function getListTinh()
{
   $url='https://thongtindoanhnghiep.co/api/city';
   $khuvuc=file_get_contents($url);
   $kv=json_decode($khuvuc);
   return $kv->LtsItem;
}
public function getListHuyen($id)
{
   $url="https://thongtindoanhnghiep.co/api/city/{$id}/district";
   $khuvuc=file_get_contents($url);
   $kv=json_decode($khuvuc);
   return $kv;
}
public static function time_ago($created_time) {
    date_default_timezone_set('Asia/Calcutta'); //Change as per your default time
    $str = strtotime($created_time);
    $today = strtotime(date('Y-m-d H:i:s'));

    // It returns the time difference in Seconds...
    $time_differnce = $today - $str;

    // To Calculate the time difference in Years...
    $years = 60 * 60 * 24 * 365;

    // To Calculate the time difference in Months...
    $months = 60 * 60 * 24 * 30;

    // To Calculate the time difference in Days...
    $days = 60 * 60 * 24;

    // To Calculate the time difference in Hours...
    $hours = 60 * 60;

    // To Calculate the time difference in Minutes...
    $minutes = 60;

    if (intval($time_differnce / $years) > 1) {
    	return intval($time_differnce / $years) . " năm trước";
    } else if (intval($time_differnce / $years) > 0) {
    	return intval($time_differnce / $years) . " năm trước";
    } else if (intval($time_differnce / $months) > 1) {
    	return intval($time_differnce / $months) . " tháng trước";
    } else if (intval(($time_differnce / $months)) > 0) {
    	return intval(($time_differnce / $months)) . " tháng trước";
    } else if (intval(($time_differnce / $days)) > 1) {
    	return intval(($time_differnce / $days)) . " ngày trước";
    } else if (intval(($time_differnce / $days)) > 0) {
    	return intval(($time_differnce / $days)) . " ngày trước";
    } else if (intval(($time_differnce / $hours)) > 1) {
    	return intval(($time_differnce / $hours)) . " giờ trước";
    } else if (intval(($time_differnce / $hours)) > 0) {
    	return intval(($time_differnce / $hours)) . " giờ trước";
    } else if (intval(($time_differnce / $minutes)) > 1) {
    	return intval(($time_differnce / $minutes)) . " phút trước";
    } else if (intval(($time_differnce / $minutes)) > 0) {
    	return intval(($time_differnce / $minutes)) . " phút trước";
    } else if (intval(($time_differnce)) > 1) {
    	return intval(($time_differnce)) . " giây trước";
    } else {
    	return "Vài giây trước";
    }
}

  public static function filter($data) {
  	$data = trim($data);
  	$data = stripslashes($data);
  	$data = htmlspecialchars($data);
  	return $data;
  }
  public static function is_login()
  {
    return !empty($_SESSION['id'])?true:false;
  }
}

?>