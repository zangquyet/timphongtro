<?php 
include "Views/partial/header.php"
?>
<div class="container" style="margin-top: 20px;">
	<div class="row">
		<div class="col-md-6 text-center">
			<i class="fa fa-user" style="font-size: 10em;"></i>
			<h3>Đăng kí mới người dùng</h3>
		</div>
		<div class="col-md-6">

			<form action="" method="POST" role="form">
				<div class="form-group label-floating">
					<label for="i5" class="control-label">Username</label>
					<input type="text" class="form-control <?=(isset($login) && $login==false)?"animated shake canhbao":""?>" name="username" placeholder="Username" required=""/>
					<span class="help-block">Tên đăng nhập</span>
				</div>

				<div class="form-group label-placeholder">
					<label for="i5p" class="control-label">Password</label>
					<input type="Password" class="form-control <?=(isset($login) && $login==false)?"animated shake canhbao":""?>"  name="pass" placeholder="Password" required=""/>  
					<span class="help-block">Mật khẩu</span>
				</div>

				<button type="submit" class="btn btn-primary btn-raised">Đăng nhập!</button>
			</form>
		</div>
	</div>
</div>
<?php 
include "Views/partial/footer.php"
?>

