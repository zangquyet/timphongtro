<?php 
include 'Views/partial/header.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<i style="font-size: 10em;color: #f1c40f" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
			<h1 style="font-weight: bold">404</h1>
			<h3>Không tìm thấy trang bạn tìm kiếm,<a href="<?php echo siteurl ?>">Quay lại trang chủ</a></h3>
		</div>
	</div>
</div>

<?php 
include 'Views/partial/footer.php';
?>