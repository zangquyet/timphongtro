<?php 
include 'Views/partial/admin_header.php';
?>


<section class="content-header">
	<h1>
		Tất cả các bài đăng
		<!--<small>preview of simple tables</small>-->
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Tất cả các bài đăng</a></li>
		<!--<li class="active">Simple</li>-->
	</ol>
</section>
<section class="content">
	<?php if (!empty($_SESSION['message'])): ?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-<?php echo $_SESSION['code']=="success"?"success":"warning" ?>">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong><?php echo $_SESSION['message'] ?></strong>
				</div>
			</div>
		</div>
		<?php
		unset($_SESSION['code']); 
		unset($_SESSION['message']); 
		?>
	<?php endif ?>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						
					</h3>

					<div class="box-tools">
						<form method="GET" action="<?php echo $siteurl ?>">
							<div class="input-group input-group-sm" style="width: 300px;">
								<input type="hidden" name="controller" value="post"> <!-- bằng gì value ấy -->
								<input type="hidden" name="action" value="danhsachtimkiem">

								<input type="text" name="key" class="form-control pull-right" placeholder="Search" value="<?= isset($_GET['key'])?$_GET['key']:"" ?>">

								<div class="input-group-btn">
									<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</form>

					</div>
				</div>



				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody><tr>
							<th>ID</th>
							<th>Tiêu đề</th>
							<th>Giá</th>
							<th>Diện tích</th>
							<th>Người đăng</th>
							<th>Ngày thêm</th>
							<th>Trạng thái</th>
							<th>Thao tác</th>
						</tr>
						<?php
						$objUser=new User;

						foreach ($post as $post): ?>
						<tr>
							<td><?php echo $post['id'] ?></td>
							<!--<td><?php //echo $post['id'] ?></td>-->
							<td><?php echo Helper::getshorterstring($post['tieude'],30); ?></td>
							<td><?php echo $post['gia'] ?></td>
							<td><?php echo $post['dientich'] ?></td>
							<td>
								<a href="<?php echo $siteurl."user/getdetail/".$post['id_nguoiadd']?>">
									<?php echo $objUser->getUsernameByID($post['id_nguoiadd'])?>
									
								</a>
							</td>
							<td><?php echo $post['add_date'] ?></td>

							<td>

								<?php if ($post['trangthai']==2): ?>
									<span class="label label-danger">Đã xóa</span>
								<?php elseif ($post['trangthai']==0): ?>
									<span class="label label-info">Đang chờ kiểm duyệt</span>
								<?php elseif ($post['trangthai']==1): ?>
									<span class="label label-success">Đã kiểm duyệt</span>
								<?php endif ?>
							</td>
							<td>

								<a class="btn btn-danger" data-toggle="modal" href='#modal-dele<?php echo $post['id'] ?>'><i class="fa fa-trash"></i></a>

								<div class="modal fade" id="modal-dele<?php echo $post['id'] ?>">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title">Modal title</h4>
											</div>
											<div class="modal-body">
												<strong>Bạn có muốn xóa <?php echo $post['tieude'] ?></strong>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<a href="<?php echo $siteurl."post/xoaduyet/".$post['id'] ?>" class="btn btn-danger">Delete</a>
												<!-- controler + hàm controller-->
											</div>
										</div>
									</div>
								</div>

								<a href="<?php echo $siteurl."post/detail/".$post['id']?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
								<a href="<?php echo $siteurl."post/duyet/".$post['id'] ?>" class="btn btn-success"><i class="fa fa-check"></i></a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody></table>
			</div>
			<!--box-body -->



		</div>
	</div>
</div>	
</section>



</div>
<?php include 'Views/partial/admin_footer.php' ?>

?>
