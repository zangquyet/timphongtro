<?php include 'Views/partial/admin_header.php';?>

<section class="content">

  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">

          <h3 class="profile-username text-center"></h3>

          <p class="text-muted text-center"><?php 
                     echo $users['username']?></p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Tổng số bài đăng</b><a class="pull-right"><?php echo $post ?></a>
            </li>
            <li class="list-group-item">
              <b>Tổng số bình luận</b> <a class="pull-right"><?php echo $post2 ?></a>
            </li>
            
          </ul>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      <!-- About Me Box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Thông tin liên lạc</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <strong><i class="fa fa-phone margin-r-5"></i> Số điện thoại</strong>

          <p class="text-muted"><?php echo $users['sodienthoai'] ?></p>

          <hr>

          <strong><i class="fa fa-map-marker margin-r-5"></i> Email</strong>

          <p class="text-muted"><?php echo $users['email'] ?></p>

         

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>


    <!-- /.col -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#activity" data-toggle="tab">Bài đăng</a></li>
          <li><a href="#timeline" data-toggle="tab">Bình luận</a></li>
        </ul>
        <div class="tab-content">
          <div class="active tab-pane" id="activity">
            <!-- Post -->
            <div class="post">
              <div class="user-block">
                <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
                <span class="username">
                  <a href="#"><?php 
                     echo $users['username']?></a>
                  <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                </span>
              </div>
              <!-- /.user-block -->
            <div>
              
            





            <div class="well">
              <?php if (count($listpost)): ?>
                <table class="table table-striped table-hover ">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Tiêu đề</th>
                      <th>Giá</th>
                      <th>Diện tích</th>
                      <th>Người đăng</th>
                      <th>Ngày thêm</th>
                      <th>Trạng thái</th>
                      <th>Thao tác</th>
                    </tr>
                  </thead>
                  <tbody>

                     <?php foreach ($listpost as $row): ?>
                      <tr>
                        <td><?= $row['id'] ?></td>
                        <td><?= $row['tieude'] ?></td>
                        <td><?= $row['gia'] ?></td>
                        <td><?= $row['dientich'] ?></td>
                        <td><?= $row['id_nguoiadd'] ?></td>
                        <td><?= $row['add_date'] ?></td>
                        <td>
                          <?php 
                          switch ($row['trangthai']) {
                            case 0:
                            ?>
                            <span class="label label-info">Đang chờ kiểm duyệt</span>
                            <?php
                            break;
                            case 1:
                            ?>
                            <span class="label label-success">Đã duyệt</span>
                            <?php
                            break;
                            case 2:
                            ?>
                            <span class="label label-danger">Đã xóa</span>
                            <?php
                            break;

                            default:
                            break;
                          }

                          ?>
                        </td>

                      <td>
                        
                        <a class="btn btn-danger" data-toggle="modal" href='#modal-delete<?= $row['id'] ?>'><i class="fa fa-trash"></i></a>
                      <div class="modal fade" id="modal-delete<?= $row['id'] ?>">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Xóa bài đăng</h4>
                            </div>
                            <div class="modal-body">
                              Bạn có muốn xóa bài đăng <strong><?= $row['tieude'] ?></strong>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                              <a type="button" class="btn btn-danger btn-raised" href="<?= $siteurl.'post/delete/'.$row['id'] ?>">Xóa</a>

                            </div>
                          </div>
                        </div>
                      </div>

                <a href="<?php echo $siteurl."post/detail/".$row['id']?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                <a href="<?php echo $siteurl."post/duyet/".$row['id'] ?>" class="btn btn-success"><i class="fa fa-check"></i></a>
                      </td>


                    </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
                <?php else: ?>
            <div class="alert alert-info">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Hiện tại bạn chưa đăng bài nào</strong>
            </div>
          <?php endif ?>
            </div>







            </div>
             
                </div>
                <!-- /.post -->

                <!-- Post -->
                
                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                    <!-- The timeline -->
                    <ul class="timeline timeline-inverse">
                      <!-- timeline time label -->
                      <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2014
                        </span>
                      </li>
                      <!-- /.timeline-label -->
                      <!-- timeline item -->
                      <li>
                        <i class="fa fa-comments bg-yellow"></i>

                        <div class="timeline-item">

                          <h3 class="timeline-header"><a href="#"><?php 
                     echo $users['username']?></a> đã bình luận</h3>

                          <div class="timeline-body">
                            
            <div class="well">
              <?php if (count($listcomment)): ?>
                <table class="table table-striped table-hover ">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Bình luận</th>
                      <th>Ngày bình luận</th>
                      <th>Bài đăng</th>
                      <th>Thao tác</th>
                    </tr>
                  </thead>
                  <tbody>

                     <?php foreach ($listcomment as $row): ?>
                      <tr>
                        <td><?= $row['id'] ?></td>
                        <td><?= $row['body'] ?></td>
                        <td><?= $row['datetime'] ?></td>
                        <td><?= $row['p_id'] ?></td>
                        

                      <td>
                        
                        <a class="btn btn-danger" data-toggle="modal" href='#modal-delete<?= $row['id'] ?>'><i class="fa fa-trash"></i></a>
                      <div class="modal fade" id="modal-delete<?= $row['id'] ?>">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Xóa bình luận</h4>
                            </div>
                            <div class="modal-body">
                              Bạn có muốn xóa bình luận <strong><?= $row['body'] ?></strong>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                              <a type="button" class="btn btn-danger btn-raised" href="<?= $siteurl.'post/delete/'.$row['id'] ?>">Xóa</a>

                            </div>
                          </div>
                        </div>
                      </div>

             
                      </td>


                    </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
                <?php else: ?>
            <div class="alert alert-info">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Hiện tại không có bình luận nào</strong>
            </div>
          <?php endif ?>
            </div>

                          </div>
                          
                        </div>
                     
                  <!-- /.tab-pane -->

                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
              <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        </section>
<?php include 'Views/partial/admin_footer.php'; ?>
