<?php 
include 'Views/partial/admin_header.php';
?>
<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4>Tổng số bài đăng</h4>
              <h3><?php echo $tongbaidang ?></h3>

            </div>
            <div class="icon">
              <i class="fa fa-list-alt "></i>
            </div>
            <a href="<?php echo $siteurl?>post/listuserpost" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h4>Tổng số bài đăng đã duyệt</h4>
              <h3><?php echo $tongbaidangdaduyet ?></h3>

            </div>
            <div class="icon">
              <i class="ion ion-clipboard"></i>
            </div>
            <a href="<?php echo $siteurl?>post/dakiemduyet" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4>Tổng số bình luận</h4>
              <h3><?php echo $tongbinhluan ?></h3>

            </div>
            <div class="icon">
              <i class="fa fa-comments"></i>
            </div>
            <a href="#" class="small-box-footer"><i class="fa fa-check"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h4>Tổng số người dùng</h4>
              <h3><?php echo $tongnguoidung ?></h3>

            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="<?php echo $siteurl?>user/listuser" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>


        </section>
        <!-- right col -->
 
<?php include 'Views/partial/admin_footer.php' ?>

?>