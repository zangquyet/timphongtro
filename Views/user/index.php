<?php 
include 'Views/partial/admin_header.php';
?>
<section class="content-header">
	<h1>
		Quản lý người dùng
		<!--<small>preview of simple tables</small>-->
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Quản lý người dùng</a></li>
		<!--<li class="active">Simple</li>-->
	</ol>
</section>

<div class="content">
	<?php if (!empty($_SESSION['message'])): ?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-<?php echo $_SESSION['code']=="success"?"success":"warning" ?>">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong><?php echo $_SESSION['message'] ?></strong>
				</div>
			</div>
		</div>
		<?php
		unset($_SESSION['code']); 
		unset($_SESSION['message']); 
		?>
	<?php endif ?>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">

				<div class="box-header">
					<h3 class="box-title">
						<a class="btn btn-primary" data-toggle="modal" href='#modal-add'>Add user</a>
						<div class="modal fade" id="modal-add">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Add user</h4>
									</div>

									<form action="<?php echo $siteurl ?>user/add" method="POST" role="form">
										<div class="modal-body">

											<table class="table">

												<tbody>
													<tr>
														<td>Username</td>
														<td><input type="text" name="username" id="input" class="form-control" value="" required="required" title=""></td>
													</tr>
													<tr>
														<td>Password</td>
														<td><input type="text" name="password" id="input" class="form-control" value="" required="required" title=""></td>
													</tr>
													<tr>
														<td>Email</td>
														<td><input type="text" name="email" id="input" class="form-control" value="" required="required"  title=""></td>
													</tr>
													<tr>
														<td>Phone</td>
														<td><input type="text" name="sodienthoai" id="input" class="form-control" value="" required="required" title=""></td>
													</tr>
													<tr>
														<td>Level</td>
														<td>
															<select name="level" id="input" class="form-control">
																<option value="">-- Select One --</option>
																<option value="1">-- Admin --</option>
																<option value="2">-- Người dùng --</option>
															</select>

														</td>
													</tr>

												</tbody>
											</table>

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Save changes</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</h3>

					<div class="box-tools">
						<form method="GET" action="<?php echo $siteurl ?>">
						<div class="input-group input-group-sm" style="width: 300px;">
								<input type="hidden" name="controller" value="user"> 
								<input type="hidden" name="action" value="listuser">

								<input type="text" name="key" class="form-control pull-right" placeholder="Search" value="<?= isset($_GET['key'])?$_GET['key']:"" ?>">

								<div class="input-group-btn">
									<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody><tr>
							<th>ID</th>
							<th>Username</th>
							<th>Email</th>
							<th>SDT</th>
							<th>Level</th>
							<th>Action</th>
						</tr>
						<?php foreach ($users as $user): ?>

							<tr>
								<td><?php echo $user['id'] ?></td>
								<td><?php echo $user['username'] ?></td>
								<td><?php echo $user['email'] ?></td>
								<td><?php echo $user['sodienthoai'] ?></td>
								<td><?php echo $user['level']==1?"ADMIN":"User" ?></td>
								<td>
									<a href="<?php echo $siteurl."user/getdetail/".$user['id'] ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
									<a class="btn btn-success" data-toggle="modal" href='#modal-edit<?php echo $user['id'] ?>'><i class="fa fa-pencil"></i></a>
									<div class="modal fade" id="modal-edit<?php echo $user['id'] ?>">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h4 class="modal-title">Edit User</h4>
												</div>
												<form action="<?php echo $siteurl ?>user/update" method="POST" role="form">
													<input type="hidden" name="id">
													<div class="modal-body">
														<table class="table table-hover">
															<input type="hidden" name="id" value="<?php echo $user['id'] ?>">
															<tbody>
																<tr>
																	<!--  bên này có 1 trường ID hidden (ẩn) values là id -->
																	<input type="hidden" name="id" value="<?php echo $user['id']?>">
																	<td>Username</td>
																	<td><input type="text" name="username" id="inputEcho" class="form-control" value="<?php echo $user['username'] ?>" required="required" title=""></td>
																</tr>
																<tr>
																	<td>Email</td>
																	<td><input type="text" name="email" id="inputEcho" class="form-control" value="<?php echo $user['email'] ?>" required="required" title=""></td>
																</tr>
																<tr>
																	<td>Phone</td>
																	<td><input type="text" name="sodienthoai" id="inputEcho" class="form-control" value="<?php echo $user['sodienthoai'] ?>"  title=""></td>
																</tr>
																<tr>
																	<td>Level</td>
																	<td>
																		<select name="level" id="inputLevel" class="form-control">
																			<option value="">-- Select One --</option>
																			<option <?php echo $user['level']==1?"selected":""; ?> value="1">-- Admin --</option>
																			<option <?php echo $user['level']==3?"selected":""; ?> value="0">-- Người dùng --</option>
																		</select></td>
																	</tr>
																</tbody>
															</table>
														</div>
														
														<div class="modal-footer">
															<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

															<!-- đây này coppy có cái data-dismis thì nó không gửi form -->
															<button type="submit" class="btn btn-success">Edit</button>
															

															<!--muốn làm gì đ-->

														</div>

													</form>
												</div>
											</div>
										</div>


										<a class="btn btn-danger" data-toggle="modal" href='#modal-del<?php echo $user['id'] ?>'><i class="fa fa-trash"></i></a>
										<div class="modal fade" id="modal-del<?php echo $user['id'] ?>">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
														<h4 class="modal-title">Modal title</h4>
													</div>
													<div class="modal-body">
														<strong>Bạn có muốn xóa <?php echo $user['username'] ?></strong>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
														<a href="<?php echo $siteurl."user/delete/".$user['id'] ?>" class="btn btn-danger">Delete</a>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody></table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<?php 
	include 'Views/partial/admin_footer.php';
	?>