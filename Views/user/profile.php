	<?php 
	include 'Views/partial/header.php';
	?>
	<script>
		function xoa(id,title) {
			var del=confirm("Bạn có muốn xóa bài đăng: "+title);
			if (del==true) {
				window.location.assign('<?= $siteurl."post/delete/"?>'+id);
			}
		}
	</script>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php include 'Views/partial/userpanel.php'; ?>
			</div>
			<div class="col-md-9">
				<?php include 'Views/partial/alert.php'; ?>
				<div class="well">
					<h3>Hồ sơ người dùng</h3>
					<form action="<?= $siteurl ?>user/updateprofile" method="POST" role="form">
						<div class="form-group">
							<label for="">Tên người dùng</label>
							<input name="fullname" type="text" value="<?= $user['fullname'] ?>" class="form-control" id="" >
						</div>
						<div class="form-group">
							<label for="">Email</label>
							<input name="email" type="text" value="<?= $user['email'] ?>" class="form-control" id="" >
						</div>
						<div class="form-group">
							<label for="">Số điện thoại</label>
							<input name="sodienthoai" type="text" value="<?= $user['sodienthoai'] ?>" class="form-control" id="" >
						</div>
					
						
					
						<button type="reset" class="btn btn-default">Reset</button>
						<button type="submit" class="btn btn-primary btn-raised">Lưu</button>
					</form>
				</div>

			</div>
		</div>
	</div>
	<?php 
	include 'Views/partial/footer.php';
	?>