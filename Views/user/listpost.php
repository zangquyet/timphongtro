	<?php 
	include 'Views/partial/header.php';
	?>
	<script>
		function xoa(id,title) {
			var del=confirm("Bạn có muốn xóa bài đăng: "+title);
			if (del==true) {
				window.location.assign('<?= $siteurl."post/delete/"?>'+id);
			}
		}
	</script>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php include 'Views/partial/userpanel.php'; ?>
			</div>
			<div class="col-md-9">
				<?php include 'Views/partial/alert.php'; ?>
				<div class="well">
					<h3>Danh sách bài đăng</h3>
					<?php if (count($listpost)): ?>
						<table class="table table-striped table-hover ">
							<thead>
								<tr>
									<th>#</th>
									<th>Tiêu đề</th>
									<th>Trạng thái</th>
									<th>Thao tác</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($listpost as $row): ?>
									<tr>
										<td><?= $row['id'] ?></td>
										<td><?= $row['tieude'] ?></td>
										<td>
											<?php 
											switch ($row['trangthai']) {
												case 0:
												?>
												<span class="label label-info">Đang chờ kiểm duyệt</span>
												<?php
												break;
												case 1:
												?>
												<span class="label label-success">Đã duyệt</span>
												<?php
												break;
												case 2:
												?>
												<span class="label label-danger">Đã xóa</span>
												<?php
												break;

												default:
												break;
											}

											?>
										</td>
										<td>
											<a class="btn btn-danger" data-toggle="modal" href='#modal-delete<?= $row['id'] ?>'><i class="fa fa-trash"></i></a>
											<a href="<?= $siteurl.'post/detail/'.$row['id'] ?>"><i class="fa fa-eye"></i></a>
											<div class="modal fade" id="modal-delete<?= $row['id'] ?>">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
															<h4 class="modal-title">Xóa bài đăng</h4>
														</div>
														<div class="modal-body">
															Bạn có muốn xóa bài đăng <strong><?= $row['tieude'] ?></strong>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
															<a type="button" class="btn btn-danger btn-raised" href="<?= $siteurl.'user/deletepost/'.$row['id'] ?>">Xóa</a>

														</div>
													</div>
												</div>
											</div>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					<?php else: ?>
						<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>Hiện tại bạn chưa đăng bài nào</strong>
						</div>
					<?php endif ?>
				</div>

			</div>
		</div>
	</div>
	<?php 
	include 'Views/partial/footer.php';
	?>