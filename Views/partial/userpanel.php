<div class="bs-component">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">User Dashboard</h3>
		</div>
		<div class="panel-body">
			<ul class="nav nav-pills nav-stacked" style="max-width: 300px;">
				<li class="<?= $active==1?"active":"" ?>"><a href="<?= $siteurl ?>user/listpost">Danh sách bài đăng</a></li>
				<li class="<?= $active==2?"active":"" ?>"><a href="<?= $siteurl ?>user/profile">Thông tin cá nhân</a></li>
				<li class="<?= $active==3?"active":"" ?>"><a href="<?= $siteurl.'user/changepass' ?>">Đổi mật khẩu</a></li>
			</ul>
		</div>
	</div>
</div>