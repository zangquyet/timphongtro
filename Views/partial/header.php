<?php 
$siteurl=Helper::$siteurl;
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tìm kiếm phòng trọ</title>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="<?= $siteurl ?>md/js/jquery.js"></script>
	<script src="<?= $siteurl ?>md/js/bootstrap.min.js"></script>
	<script src="<?= $siteurl ?>md/js/material.min.js"></script>
	<script src="<?= $siteurl ?>md/js/ripples.min.js"></script>
	<script src="<?= $siteurl ?>md/js/snackbar.min.js"></script>
	<script src="<?= $siteurl ?>md/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
	<script src="<?= $siteurl ?>slick/slick.min.js"></script>
	<link rel="stylesheet" href="<?= $siteurl ?>md/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= $siteurl ?>md/css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="<?= $siteurl ?>md/css/ripples.min.css">
	<link rel="stylesheet" href="<?= $siteurl ?>md/css/snackbar.min.css">
	<link rel="stylesheet" href="<?= $siteurl ?>md/css/ion.rangeSlider.skinModern.css">
	<link rel="stylesheet" href="<?= $siteurl ?>md/css/ion.rangeSlider.css">
	<link rel="stylesheet" href="<?= $siteurl ?>md/css/style.css">
	<link rel="stylesheet" href="<?= $siteurl ?>slick/slick.css">

</head>
<script>
	$(document).ready(function() {
		$.material.init();
	});
</script>
<body>
	<header>
		<div id="logo">
			<div class="container">
				<a href="<?= $siteurl ?>"><img src="<?= $siteurl ?>images/logo.png" style="max-width: 350px;
					padding: 20px 0px" class="img-responsive" alt="Image"></a>
				</div>
			</div>
			<div class="bs-component">
				<div class="navbar navbar-default">
					<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="<?= $siteurl ?>">BOCONGANH</a>
						</div>
						<div class="navbar-collapse collapse navbar-responsive-collapse">
							<ul class="nav navbar-nav">
								<li class="active"><a href="javascript:void(0)">Trang chủ</a></li>
								<li><a href="<?= $siteurl ?>page/contact"><i class="fa fa-address-book"></i> Liên lạc</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><a href="<?= $siteurl ?>post/add"><i class="fa fa-cloud-upload"></i> Thêm bài mới</a></li>
								<?php if (!Helper::is_login()): ?>
									<li><a href="<?= $siteurl ?>page/login"><i class="fa fa-cloud-upload"></i> Đăng nhập</a></li>
								<?php else: ?>

									
										<li class="dropdown">
										<a href="" data-target="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-user"></i> <?= $_SESSION['username'] ?>
											<b class="caret"></b><div class="ripple-container"></div></a>
											<ul class="dropdown-menu">
												<li><a href="<?= $siteurl ?>user/listpost">Control Panel</a></li>
												<?php if (Helper::isAdmin()): ?>
													<li><a href="<?= $siteurl ?>user/dashboard">Admin Control Panel</a></li>
												<?php endif ?>
												<li><a href="<?= $siteurl ?>user/changepass">Đổi mật khẩu</a></li>
												<li class="divider"></li>
												<li><a href="<?= $siteurl ?>/page/logout">Đăng xuất</a></li>
											</ul>
										</li>
									<?php endif ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</header>
