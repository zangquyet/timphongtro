<?php if (!empty($_SESSION['message'])): ?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-<?php echo $_SESSION['code']=="success"?"success":"warning" ?>">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong><?php echo $_SESSION['message'] ?></strong>
				</div>
			</div>
		</div>
	<?php endif ?>
		<?php

		unset($_SESSION['code']); 
		unset($_SESSION['message']); 
		?>