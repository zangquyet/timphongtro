<?php
include 'Views/partial/header.php'; 
   //Helper::getHeader($title);
?>
<script>
	function update_district() {
		$("#district").html("<option value=\"\">Vui lòng đợi</option>");
		$.ajax({
			url: '/mvc/city_ajax.php',
			type: 'get',
			dataType: 'text',
			data: {
				action: 'getdistrict'
				,id:$("#province").val()
			},
		})
		.done(function(result) {
			console.log("success");
   		//console.log(result);
   		//var obj = JSON.parse(result);
   		//console.log(obj.length);
   		//console.log(result);
   		$("#district").html(result);
   		//$("#district").html("Hello <b>world</b>!");
   	})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	}
	function handleFileSelect() {
		$("#result").empty();
                    //Check File API support
                    if (window.File && window.FileList && window.FileReader) {

                        var files = event.target.files; //FileList object
                        var output = document.getElementById("result");
                        if (files.length>6) {
                        	alert("Số file upload không được quá 6 ảnh");
                        	$("#file").val("");
                        	return;
                        }
                        for (var i = 0; i < files.length; i++) {
                        	var file = files[i];
                            //Only pics
                            if (!file.type.match('image')) continue;

                            var picReader = new FileReader();
                            picReader.addEventListener("load", function (event) {
                            	var picFile = event.target;
                            	var div = document.createElement("div");
                            	div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" + "title='" + picFile.name + "'/>";
                            	output.insertBefore(div, null);
                            });
                            //Read the image
                            picReader.readAsDataURL(file);
                        }
                    } else {
                    	console.log("Your browser does not support File API");
                    }
                }

                function opendialog() {
                	$('input[type=file]').click();
                }
            </script>
            <form action="<?= $siteurl ?>post/add" method="POST" enctype="multipart/form-data">
            	<div class="container">
            		<div class="row">
            			<div class="col-md-6">
            				<div class="panel panel-primary">
            					<div class="panel-heading">
            						Thông tin chung
            					</div>
            					<div class="panel-body">
            						<div class="col-md-12">
            							<div class="form-group">
            								<label for="">Tiêu đề tin</label>
            								<input type="text" value="<?= $thispost['tieude'] ?>" name="tieude" class="form-control" id="" placeholder="">
            							</div>
            						</div>
            						<div class="col-md-12">
            							<div class="form-group">
            								<label for="">Số điện thoại</label>
            								<input type="text" value="<?= $thispost['sodienthoai'] ?>" name="sodienthoai" class="form-control" id="" placeholder="">
            							</div>
            						</div>
            						<div class="col-md-6">
            							<div class="form-group">
            								<label for="">Giá cho thuê</label>
            								<input type="text" value="<?= $thispost['gia']/1000000 ?>" name="gia" class="form-control" id="" placeholder="">
            							</div>
            						</div>
            						<div class="col-md-6">
            							<div class="form-group">
            								<label for="">Đơn vị</label>
            								<select name="donvi" class="form-control" id="" placeholder="">
            									<option selected="" value="trieu">Triệu/Tháng</option>
            									<option value="nghin">Trăm/Tháng</option>
            								</select>
            							</div>
            						</div>
            						<div class="col-md-12">
            							<div class="form-group">
            								<label for="">Diện tích <b>(m2)</b></label>
            								<input name="dientich" value="<?= $thispost['dientich'] ?>" type="number" class="form-control" id="" placeholder="">
            							</div>
            						</div>
            						<div class="col-md-12">
            							<div class="form-group">
            								<script>
            									$('.textarea').wysihtml5();
            								</script>
            								<script type="text/javascript" charset="utf-8">
            									$(prettyPrint);
            								</script>
            								<label for="">Mô tả chung</label>
            								<textarea name="mota" class="textarea form-control" name="mota" placeholder="Enter text ..." rows="7"><?= $thispost['mota'] ?></textarea>
            							</div>
            						</div>
            					</div>
            				</div>

            			</div>
            			<div class="col-md-6">
            				<div class="panel panel-primary">
            					<div class="panel-heading">
            						Địa chỉ
            					</div>
            					<div class="panel-body">
            						<div class="col-md-6">
            							<div class="form-group">
            								<label for="">Tỉnh/Thành phố</label>
            								<?php 
            								$province=file_get_contents('https://thongtindoanhnghiep.co/api/city');
            								$province=json_decode($province);
                           //print_r($province);
            								?>
            								<select name="tinh_id" id="province" class="form-control" onchange="update_district()">
            									<option value="">Chọn Tỉnh/Thành phố</option>
            									<?php 
            									foreach ($province->LtsItem as $value) {
            										if ($value->ID==$thispost['tinh_id']) {
            											echo "<option selected=\"\" value=\"{$value->ID}\">{$value->Title}</option>";
            										}else {
            											echo "<option value=\"{$value->ID}\">{$value->Title}</option>";
            										}

            									}
            									?>
            								</select>
            							</div>
            						</div>
            						<div class="col-md-6">
            							<div class="form-group">
            								<label for="">Huyện/Quận <span id="value"></span> </label>
            								<select name="huyen_id" id="district" class="form-control">
            									<option value="">Chọn Quận/	Huyện</option>
            									<?php 
            									foreach (Helper::getlistHuyen($thispost['tinh_id']) as $value): ?>
            										<?php if ($value->ID==$thispost['huyen_id']): ?>
            											<option selected="" value=""><?= $value->Title ?></option>
            										<?php else: ?>
            											<option value=""><?= $value->Title ?></option>
            										<?php endif ?>
            									<?php endforeach ?>
            								</select>
            							</div>
            						</div>
            						<div class="col-md-12">
            							<div class="form-group">
            								<label for="">Địa chỉ <span id="value"></span> </label>
            								<input name="diachi" value="<?= $thispost['diachi'] ?>" type="text" name="" id="input" class="form-control" value="">
            							</div>
            						</div>
            					</div>
            				</div>
            				<div class="panel panel-primary">
            					<div class="panel-heading">
            						Hình ảnh
            					</div>

            					<div class="panel-body">

            						<button type="button" onclick="opendialog()" class="btn btn-primary">Click vào để thay đổi ảnh</button>
            						<input type="file" name="image[]" style="visibility: hidden;" multiple="" onchange="handleFileSelect()" id="file" class="form-control" value="" >
            					</div>
            					<div id="result">
            						<?php
            						$collection=Helper::getImage($thispost['img_folder']);
            						 foreach ($collection as $value): ?>
            							<div>
            								<img class="thumbnail" src="<?= $siteurl."uploads/".$thispost['img_folder']."/".$value ?>" alt="">
            							</div>
            						<?php endforeach ?>
            					</div>
            				</div>
            			</div>
            		</div>


            		<div class="row">
            			<div class="col-md-12">
            				<input type="submit" value="PUSH" class="btn btn-success btn-block">
            			</div>
            		</div>

            	</div>
            </form>
            <?php 
            include 'Views/partial/footer.php';
            ?>