<?php
include 'Views/partial/header.php'; 
   //Helper::getHeader($title);
?>
<script>
	$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

    	var $target = $(e.target);

    	if ($target.parent().hasClass('disabled')) {
    		return false;
    	}
    });

    $(".next-step").click(function (e) {

    	var $active = $('.wizard .nav-tabs li.active');
    	$active.next().removeClass('disabled');
    	nextTab($active);

    });
    $(".prev-step").click(function (e) {

    	var $active = $('.wizard .nav-tabs li.active');
    	prevTab($active);

    });
});

	function nextTab(elem) {
		$(elem).next().find('a[data-toggle="tab"]').click();
	}
	function prevTab(elem) {
		$(elem).prev().find('a[data-toggle="tab"]').click();
	}
	function update_district() {
		$("#district").html("<option value=\"\">Vui lòng đợi</option>");
		$.ajax({
			url: '/timphongtro/city_ajax.php',
			type: 'get',
			dataType: 'text',
			data: {
				action: 'getdistrict'
				,id:$("#province").val()
			},
		})
		.done(function(result) {
			console.log("success");
   		//console.log(result);
   		//var obj = JSON.parse(result);
   		//console.log(obj.length);
   		//console.log(result);
   		$("#district").html(result);
   		//$("#district").html("Hello <b>world</b>!");
   	})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	}
	function handleFileSelect() {
		$("#result").empty();
                    //Check File API support
                    if (window.File && window.FileList && window.FileReader) {

                        var files = event.target.files; //FileList object
                        var output = document.getElementById("result");
                        if (files.length>6) {
                        	alert("Số file upload không được quá 6 ảnh");
                        	$("#file").val("");
                        	return;
                        }
                        for (var i = 0; i < files.length; i++) {
                        	var file = files[i];
                            //Only pics
                            if (!file.type.match('image')) continue;

                            var picReader = new FileReader();
                            picReader.addEventListener("load", function (event) {
                            	var picFile = event.target;
                            	var div = document.createElement("div");
                            	div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" + "title='" + picFile.name + "'/>";
                            	output.insertBefore(div, null);
                            });
                            //Read the image
                            picReader.readAsDataURL(file);
                        }
                    } else {
                    	console.log("Your browser does not support File API");
                    }
                }

                function opendialog() {
                	$('input[type=file]').click();
                }
            </script>
            <form action="<?= $siteurl ?>post/add" method="POST" enctype="multipart/form-data">
            	
            	<div class="container">
            		<div class="row">
            			<section>
            				<div class="wizard">
            					<div class="wizard-inner">
            						<div class="connecting-line"></div>
            						<ul class="nav nav-tabs" role="tablist">

            							<li role="presentation" class="active">
            								<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Thông tin chung">
            									<span class="round-tab">
            										<i class="fa fa-info"></i>
            									</span>
            								</a>
            							</li>

            							<li role="presentation" class="disabled">
            								<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Địa chỉ">
            									<span class="round-tab">
            										<i class="fa fa-map-marker"></i>
            									</span>
            								</a>
            							</li>
            							<li role="presentation" class="disabled">
            								<a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Lựa chọn hình ảnh">
            									<span class="round-tab">
            										<i class="fa fa-picture-o"></i>
            									</span>
            								</a>
            							</li>

            							<li role="presentation" class="disabled">
            								<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Hoàn thành">
            									<span class="round-tab">
            										<i class="fa fa-check"></i>
            									</span>
            								</a>
            							</li>
            						</ul>
            					</div>

            					<form role="form">
            						<div class="tab-content">
            							<div class="tab-pane active" role="tabpanel" id="step1">
            								<h3>Bước 1: Thông tin chung</h3>
            								<div class="col-md-12">
            									<div class="form-group">
            										<label for="">Tiêu đề tin</label>
            										<input type="text" name="tieude" class="form-control" id="" placeholder="">
            									</div>
            								</div>
            								<div class="col-md-12">
            									<div class="form-group">
            										<label for="">Số điện thoại</label>
            										<input type="text" name="sodienthoai" class="form-control" id="" placeholder="">
            									</div>
            								</div>
            								<div class="col-md-6">
            									<div class="form-group">
            										<label for="">Giá cho thuê</label>
            										<input type="number" name="gia" class="form-control" id="" placeholder="">
            									</div>
            								</div>
            								<div class="col-md-6">
            									<div class="form-group">
            										<label for="">Đơn vị</label>
            										<select name="donvi" class="form-control" id="" placeholder="">
            											<option value="trieu">Triệu/Tháng</option>
            											<option value="nghin">Nghìn/Tháng</option>
            										</select>
            									</div>
            								</div>
            								<div class="col-md-12">
            									<div class="form-group">
            										<label for="">Diện tích <b>(m2)</b></label>
            										<input name="dientich" type="number" class="form-control" id="" placeholder="">
            									</div>
            								</div>
            								<div class="col-md-12">
            									<div class="form-group">

            										<label for="">Mô tả chung</label>
            										<textarea name="mota" class="textarea form-control" name="mota" placeholder="Enter text ..." rows="7"></textarea>
            									</div>
            								</div>
            								<ul class="list-inline pull-right">
            									<li><button type="button" class="btn btn-primary next-step btn-raised">Tiếp theo</button></li>
            								</ul>
            							</div>
            							<div class="tab-pane" role="tabpanel" id="step2">
            								<h3>Step 2</h3>
            								<div class="col-md-6">
            									<div class="form-group">
            										<label for="">Tỉnh/Thành phố</label>
            										<?php 
            										$province=file_get_contents('https://thongtindoanhnghiep.co/api/city');
            										$province=json_decode($province);
            										?>
            										<select name="tinh_id" id="province" class="form-control" onchange="update_district()">
            											<option value="">Chọn Tỉnh/Thành phố</option>
            											<?php 
            											foreach ($province->LtsItem as $value) {
            												echo "<option value=\"{$value->ID}\">{$value->Title}</option>";
            											}
            											?>
            										</select>
            									</div>
            								</div>
            								<div class="col-md-6">
            									<div class="form-group">
            										<label for="">Huyện/Quận <span id="value"></span> </label>
            										<select name="huyen_id" id="district" class="form-control">
            											<option value="">Chọn Quận/	Huyện</option>
            										</select>
            									</div>
            								</div>
            								<div class="col-md-12">
            									<div class="form-group">
            										<label for="">Địa chỉ <span id="value"></span> </label>
            										<input name="diachi" type="text" name="" id="input" class="form-control" value="">
            									</div>
            								</div>
            								<ul class="list-inline pull-right">
            									<li><button type="button" class="btn btn-default prev-step">Lùi lại</button></li>
            									<li><button type="button" class="btn btn-primary next-step btn-raised">Bước tiếp theo</button></li>
            								</ul>
            							</div>
            							<div class="tab-pane" role="tabpanel" id="step3">
            								<h3>Step 3</h3>

            								<button type="button" onclick="opendialog()" class="btn btn-primary">Click vào để chọn ảnh</button>
            								<input type="file" name="image[]" style="visibility: hidden;" multiple="" onchange="handleFileSelect()" id="file" class="form-control" value="" >
            								<ul class="list-inline pull-right">
            									<li><button type="button" class="btn btn-default prev-step">Lùi lại</button></li>
            									<li><button type="button" class="btn btn-default next-step">Bỏ qua</button></li>
            									<li><button type="button" class="btn btn-primary btn-info-full next-step btn-raised">Bước tiếp theo</button></li>
            								</ul>
            							</div>
            							<div class="tab-pane" role="tabpanel" id="complete">
            								<h3>Complete</h3>
            								<p>Bạn có muốn publish bài đăng này</p>
            								<button class="btn btn-primary btn-raised" type="submit">Pushlih</button>
            							</div>
            							<div class="clearfix"></div>
            						</div>
            					</form>
            				</div>
            			</section>
            		</div>
            	</div>


            	
            </form>
            <?php 
            include 'Views/partial/footer.php';
            ?>