<?php
include 'views/partial/header.php';
?>
<script>
	function update_district() {
		$("#district").html("<option value=\"\">Vui lòng đợi</option>");
		$.ajax({
			url: 'city_ajax.php',
			type: 'get',
			dataType: 'text',
			data: {
				action: 'getdistrict'
				,id:$("#province").val()
			},
		})
		.done(function(result) {
			console.log("success");
   		//console.log(result);
   		//var obj = JSON.parse(result);
   		//console.log(obj.length);
   		//console.log(result);
   		$("#district").html(result);
   		//$("#district").html("Hello <b>world</b>!");
   	})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	}

	$(document).ready(function() {
		
		$("#gia").ionRangeSlider({
			type: "double",
			min: 0,
			max: 20,
			step: 0.5,
			grid: true,
			grid_snap: true,
			postfix:" triệu đồng"
		});

	});
</script>



<div class="container">
	<form action="<?= $siteurl ?>">
		<div class="card">
			<div class="row text-center">
				<h2>Miễn phí, Tiện lợi</h2>
			</div>
			<div class="row" style="padding: 10px">
				<div class="col-md-3">
					<select name="tinh_id" onchange="update_district()" id="province" class="form-control" required="">

						<option value="">-- Chọn Tỉnh/Thành phố --</option>
						<?php foreach (Helper::getListTinh() as $tinh): ?>
							<option value="<?= $tinh->ID ?>"><?= $tinh->Title ?></option>

						<?php endforeach ?>
					</select>
				</div>
				<div class="col-md-3">
					<select name="huyen_id" id="district" class="form-control">
						<option value="">-- Chọn Quận/Huyện--</option>


					</select>
				</div>
				<div class="col-md-6">
					<input type="text" id="gia" name="gia" value="" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-push-4">
				<button type="submit" name="timkiem" value="true" class="btn btn-block btn-lg btn-raised btn-success"><span class="fa fa-search"></span> Tìm kiếm</button>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="container" style="margin-top: 20px;">
	<?php if (!empty($_GET['timkiem'])): ?>
		Kết quả tìm kiếm cho <?= Helper::getKhuVuc($_) ?>
	<?php endif ?>
	<?php if (count($allpost)): ?>
		<div class="row">
		<?php foreach ($allpost as $row):
		$img =Helper::getImage($row['img_folder']);
		$imgurl=$siteurl.'uploads/'.$row['img_folder']."/".array_shift($img);
		?>
		<div class="col-md-3">
			<div class="card myitem">
				<div class="khung">
					<a href="<?= $siteurl."post/detail/".$row['id'] ?>"><img src="<?= $imgurl ?>" alt=""></a>
					<div class="gia">
						<i class="fa fa-tags"></i> <?= number_format($row['gia']); ?> <sup>đ</sup>
					</div>
				</div>
				<div class="khung-footer">
					<p><i class="fa fa-tags"></i> <?= $row['diachi'] ?></p>
					<p><i class="fa fa-map-marker"></i> <?php 
						$url='https://thongtindoanhnghiep.co/api/district/'.$row['huyen_id'];
						$khuvuc=file_get_contents($url);
						$kv=json_decode($khuvuc);
						echo $kv->Title." , ".$kv->TinhThanhTitle;
						?></p>
					</div>
				</div>
			</div>	
		<?php endforeach ?>

	</div>
	<?php else: ?>
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Không có kết quả phù hợp</strong> 
		</div>
	<?php endif ?>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= $phantrang ?>
		</div>
	</div>
</div>



<?php
include 'views/partial/footer.php';
?>