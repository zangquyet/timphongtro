<?php 
include "Views/partial/header.php"
?>

<div class="container">
	<div class="card">
		<div class="container-fliud">
			<div class="wrapper row">
				<div class="preview col-md-6">

					<div class="preview-pic tab-content">
						<?php 
						$image=Helper::getImage($post['img_folder']);
						$first=array_shift($image);
						?>
						<div class="tab-pane active" id="pic-1"><img src="<?php echo $siteurl."uploads/".$post['img_folder']."/".$first ?>" /></div>
						<?php
						$i=2;
						foreach ($image as $img): ?>


						<div class="tab-pane" id="pic-<?=$i++?>"><img src="<?php echo $siteurl."uploads/".$post['img_folder']."/".$img ?>" /></div>

					<?php endforeach ?>

				</div>
				<ul class="preview-thumbnail nav nav-tabs">
					<li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="<?php echo $siteurl."uploads/".$post['img_folder']."/".$first ?>" /></a></li>
					<?php
					$i=2;
					foreach ($image as $img): ?>

					<li><a data-target="#pic-<?=$i++?>" data-toggle="tab"><img src="<?php echo $siteurl."uploads/".$post['img_folder']."/".$img ?>" /></a></li>
				<?php endforeach ?>

			</ul>

		</div>
		<div class="details col-md-6">
			<h3 class="product-title"><?= $post['tieude'] ?></h3>
			<div class="rating">
				<div class="stars">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
				</div>
				<span class="review-no">41 reviews</span>
			</div>
			<p class="product-description">
				<?=$post['mota']?>
			</p>
			<h4 class="price">Mức giá: <span><?php
				echo number_format($post['gia'])." đồng/tháng";
				?></span></h4>
				<p class="vote"><strong><i class="fa fa-map-marker"></i> Địa chỉ</strong> <?= $post['diachi'] ?></p>
				<p class="price"><strong><i class="fa fa-square"></i> Diện tích</strong> <span><?= $post['dientich'] ?> <strong>m<sup>2</sup></strong></span></p>
				<p class="vote"><strong><i class="fa fa-map"></i> Khu vực</strong> <?php 
					echo Helper::getKhuVuc($post['huyen_id']);
					?></p>
					<p class="vote"><strong><i class="fa fa-user"></i> Người đăng: </strong><?php 
						$objUser=new User;
						$user=$objUser->getUserById($post['id_nguoiadd']);
						echo $user['username'];
						?>
					</p>
					<p class="vote"><strong><i class="fa fa-clock-o"></i> Đăng cách đây: </strong> <?= Helper::time_ago($post['add_date']) ?></p>
					<div class="action">
						<button class="add-to-cart btn btn-default" type="button" data-toggle="modal" href='#modal-id'>Liên hệ người cho thuê</button>
						<div class="modal fade" id="modal-id">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Thông tin người cho thuê</h4>
									</div>
									<div class="modal-body">
										<table style="width: 100%">
											<tr>
												<td>Tên: </td>
												<td><strong>Ông ABC</strong></td>
											</tr>
											<tr>
												<td>Số điện thoại: </td>
												<td><strong><?= $post['sodienthoai'] ?></strong></td>
											</tr>
										</table>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
									</div>
								</div>
							</div>
						</div>
						<button class="like btn btn-default" type="button"><span class="fa fa-heart"></span></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3>Bình luận</h3>
		</div>

		<div id="comment">
			<?php 
			$objUser=new User;

			?>
			<?php if (count($comments)): ?>

				<?php foreach ($comments as $cm): ?>
					<div class="col-md-1">
						<img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
					</div>
					<div class="col-md-11">
						<div class="panel panel-default">
							<div class="panel-heading">
								<strong><?php 
									$user=$objUser->getUserById($post['id_nguoiadd']);
									echo $user['username'];
									?></strong> <span class="text-muted">đã bình luận <?= Helper::time_ago($cm['datetime']) ?></span>
								</div>
								<div class="panel-body">
									<?= $cm['body'] ?>
								</div><!-- /panel-body -->
							</div><!-- /panel panel-default -->
						</div>
					<?php endforeach ?>	
				<?php else: ?>
					<div class="col-md-12">
						<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>Chưa có bình luận nào</strong> ...
						</div>
					</div>
				<?php endif ?>
			</div>
			<?php if (Helper::is_login()): ?>
				<div class="col-md-12">
					<div class="widget-area no-padding blank">
						<div class="status-upload">
							<form action="<?= $siteurl."post/addcomment" ?>" method="POST">
								<input type="hidden" name="id" value="<?= $post['id'] ?>">
								<textarea name="body" placeholder="Nhập bình luận của bạn vào đây" ></textarea>
								<button type="submit" class="btn btn-success btn-raised"><i class="fa fa-paper-plane"></i> Comment</button>
								</form
							</div><!-- Status Upload  -->
						</div><!-- Widget Area -->
					</div>
				<?php else: ?>
					<div class="col-md-12">
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>Bạn cần đăng nhập để bình luận</strong> 
						</div>
					</div>
				<?php endif ?>

			</div>
		</div>
	</div>





</div>
<div class="container">
	<div class="col-md-12">
		<h3>Các bài đăng cùng khu vực</h3>
	</div>
</div>
<div class="container" style="margin-top: 20px;">
	<div class="row">
		<?php foreach ($relatives as $row):
		$img =Helper::getImage($row['img_folder']);
		$imgurl=$siteurl.'uploads/'.$row['img_folder']."/".array_shift($img);
		?>
		<div class="col-md-3">
			<div class="card myitem">
				<div class="khung">
					<img src="<?= $imgurl ?>" alt="">
					<div class="gia">
						<i class="fa fa-tags"></i> <?= number_format($row['gia']); ?> <sup>đ</sup>
					</div>
				</div>
				<div class="khung-footer">
					<p><i class="fa fa-tags"></i> <?= $row['diachi'] ?></p>
					<p><i class="fa fa-map-marker"></i> <?php 
						$url='https://thongtindoanhnghiep.co/api/district/'.$row['huyen_id'];
						$khuvuc=file_get_contents($url);
						$kv=json_decode($khuvuc);
						echo $kv->Title." , ".$kv->TinhThanhTitle;
						?></p>
					</div>
				</div>
			</div>	
		<?php endforeach ?>

	</div>
</div>


<?php 
include "Views/partial/footer.php"
?>