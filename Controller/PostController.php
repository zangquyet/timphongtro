<?php 
/**
* 
*/
class PostController
{
	public function index()
	{
		$totalpage=1;
		$objPost=new Post();
		if (!empty($_GET['id'])) {
			$page=$_GET['id'];
		}else{
			$page=1;
		}
		if (!empty($_GET['timkiem']) &&$_GET['timkiem']) {
			$huyen=!empty($_GET['huyen_id'])?$_GET['huyen_id']:0;
			$tinh_id=$_GET['tinh_id'];
			$gia=$_GET['gia'];
			$gia=explode(';', $gia);
			$low=array_shift($gia)*1000000;
			$high=array_shift($gia)*1000000;
			$allpost=$objPost->search($tinh_id,$huyen,$low,$high,$page);
			$totalpage=getPageSearch($tinh_id,$huyen,$low,$high,$page);

		}else{
			$allpost=$objPost->all($page);
			if (count($allpost)==0) {
				Helper::redirect(Helper::$siteurl);
			$totalpage=$objPost->getpage();
			}
		}
		
		$url=Helper::$siteurl."post/index/";
		$phantrang=Helper::getPagination($url,$page,$totalpage);
		include 'views/post/index.php';
		//parent::loadview('post.index',$allpost);
	}
	public function detail()
	{
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			
			$objPost=new Post;
			if (!$objPost->issetPost($id)) {
				Helper::redirect(Helper::$siteurl);
			}
			$post=$objPost->getPost($id);
			$title=$post['tieude'];

			$objComment=new Comment;
			$comments=$objComment->getComment($post['id']);
			$where="id not in({$post['id']})";
			$newestposts=$objPost->getList($where);
			$relatives=$objPost->getRelativeList($post['huyen_id']);
			$title=$post['tieude'];
			include 'Views/post/detail.php';
		}else {
			Helper::redirect(Helper::$siteurl);
		}

	}


	public function add()
	{
		session_start();
		if (!Helper::is_login()) {
			Helper::redirect(Helper::$siteurl."page/login");
		}
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$donvi=$_POST['donvi'];
			$gia=$_POST['gia'];
			if ($donvi=='trieu') {
				$gia=$gia*1000000;
			}else{
				$gia=$gia*100000;
			}
			$folder='uploads/';
			while (true) {
				$img_folder=rand();
				$img_folder=md5($img_folder);
				if (!is_dir($folder.$img_folder)) {
					break;
				}
			}


			mkdir($folder.$img_folder);
			$mine=array('jpg','png','gif');
			$totalfiles=count($_FILES['image']['name']);
			for ($i=0; $i < $totalfiles; $i++) { 
				$filename=$folder.$img_folder."/".$_FILES['image']['name'][$i];
				echo $filename;
				$ext=explode('.', $_FILES['image']['name'][$i]);
				$ext=end($ext);
				if (in_array($ext, $mine)) {
					move_uploaded_file($_FILES['image']['tmp_name'][$i], $filename);
				}
			}

			$data=array(
				'tieude'=>$_POST['tieude'],
				'dientich'=>$_POST['dientich'],
				'mota'=>$_POST['mota'],
				'sodienthoai'=>$_POST['sodienthoai'],
				'tinh_id'=>$_POST['tinh_id'],
				'huyen_id'=>$_POST['huyen_id'],
				'diachi'=>$_POST['diachi'],
				'img_folder'=>$img_folder,
				'gia'=>$gia,
				'id_nguoiadd'=>$_SESSION['id']
				);

			$objPost=new Post();
			if ($objPost->add($data)) {
				$id=$objPost->getPostbyfolder($img_folder);
				echo $img_folder;
				//echo Helper::$siteurl."post/detail/".$id['id'];
				Helper::redirect(Helper::$siteurl."post/detail/".$id['id']);
			}else{
				echo "INSERT FALSE";
			}
			return;
		}
		$title="Thêm bài đăng mới";
		$objUser= new User();
		include 'views/post/add.php';
	}
	public function edit()
	{
		$objPost=new Post;
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$donvi=$_POST['donvi'];
			$gia=$_POST['gia'];
			if ($donvi=='trieu') {
				$gia=$gia*1000000;
			}else{
				$gia=$gia*100000;
			}
			$folder='uploads/';
			while (true) {
				$img_folder=rand();
				$img_folder=md5($img_folder);
				if (!is_dir($folder.$img_folder)) {
					break;
				}
			}


			mkdir($folder.$img_folder);
			$mine=array('jpg','png','gif');
			$totalfiles=count($_FILES['image']['name']);
			for ($i=0; $i < $totalfiles; $i++) { 
				$filename=$folder.$img_folder."/".$_FILES['image']['name'][$i];
				echo $filename;
				$ext=explode('.', $_FILES['image']['name'][$i]);
				$ext=end($ext);
				if (in_array($ext, $mine)) {
					move_uploaded_file($_FILES['image']['tmp_name'][$i], $filename);
				}
			}
			SESSION::start();

			$data=array(
				'tieude'=>$_POST['tieude'],
				'dientich'=>$_POST['dientich'],
				'mota'=>$_POST['mota'],
				'sodienthoai'=>$_POST['sodienthoai'],
				'tinh_id'=>$_POST['tinh_id'],
				'huyen_id'=>$_POST['huyen_id'],
				'diachi'=>$_POST['diachi'],
				'img_folder'=>$img_folder,
				'gia'=>$gia,
				'id_nguoiadd'=>$_SESSION['id']
				);

			$objPost=new Post();
			if ($objPost->add($data)) {
				$id=$objPost->getPostbyfolder($img_folder);
				echo $img_folder;
				//echo Helper::$siteurl."post/detail/".$id['id'];
				Helper::redirect(Helper::$siteurl."post/detail/".$id['id']);
			}else{
				echo "INSERT FALSE";
			}
		}
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objPost->issetPost($id)) {
				$thispost=$objPost->getPost($id);
				include 'Views/post/edit.php';
			}else {
				Helper::redirect(Helper::$siteurl);
			}
		}else {
			Helper::redirect(Helper::$siteurl);
		}
	}
	public function delete()
	{
		SESSION::start();
		//echo $_GET['id'];
		$objPost=new Post;
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objPost->issetPost($id)) {
				$post=$objPost->getPost($id);
				if ($objPost->delete($id)) {
					echo "Xóa thành công. <a href=\"".Helper::$siteurl."user/listpost\">Click để quay lại</a>";
				}else {
					echo "XÓa thất bại";
				}

			}else{
				Helper::redirect(Helper::$siteurl."error");
			}
		}
	}

	public function duyet()
	{
		SESSION::start();
		$objPost=new Post;
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objPost->issetPost($id)) {
				if ($objPost->duyet($id)) {
					session_start();
					$_SESSION['message']="đã duyệt {$id}";
					$_SESSION['code']="success";
					Helper::redirect(Helper::$siteurl."post/listuserpost");
				}
			}
		}
	}
	/*public function userdetailduyet()
	{
		SESSION::start();
		$objPost=new Post;
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objPost->issetPost($id)) {
				if ($objPost->duyet($id)) {
					session_start();
					$_SESSION['message']="đã duyệt {$id}";
					$_SESSION['code']="success";
					Helper::redirect(Helper::$siteurl."user/getdetail");
				}
			}
		}
	}*/
	
	public function xoaduyet()
	{
		SESSION::start();
		$objPost=new Post;
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objPost->issetPost($id)) {
				if ($objPost->xoaduyet($id)) {
					session_start();
					$_SESSION['message']="Di chuyển {$id} vào thùng rác";
					$_SESSION['code']="success";
					Helper::redirect(Helper::$siteurl."post/listuserpost");
				}
			}
		}
	}
	public function addcomment()
	{
		$objComment=new Comment;
		session_start();
		$id=$_POST['id'];
		$body=$_POST['body'];
		$u_id=$_SESSION['id'];
		$data=array(
			'body'=>$body,
			'u_id'=>$u_id,
			'p_id'=>$id
		);
		if ($objComment->add($data)) {
			Helper::redirect(Helper::$siteurl.'post/detail/'.$id."#comment");
		}else {
			echo "Error";
		}
	}

 	public function danhsachtimkiem()
 	{
 		if (!empty($_GET['page'])) {
			$page=$_GET['page'];
		}else{
			$page=1;
		}
 		SESSION::start();
		$objPost=new Post;
		
		if (!empty($_GET['key'])) {
			$post=$objPost->timkiembaidang($_GET['key'],$page);

		}else{
			$post=$objPost->allpost($page);
		}
		include 'Views/user/listuserpost.php';
 	}
	
 	

	public function listuserpost()
	{
		if (!empty($_GET['page'])) {
			$page=$_GET['page'];
		}else{
			$page=1;
		}
		SESSION::start();
		$objPost=new Post;
		/*if (!$objUser->islogin()) {
			include 'Views/pages/permission.php';
			return;
		}*/
		
		$post = $objPost->allpost($page);
		
		include 'Views/user/listuserpost.php';

	}

	
	public function dangchokiemduyet()
	{
		if (!empty($_GET['page'])) {
			$page=$_GET['page'];
		}else{
			$page=1;
		}
		SESSION::start();
		$objPost=new Post;
		if (!empty($_GET['key'])) {
			$post=$objPost->timkiembaidang($_GET['key'],$page);
		}
		else{
			$post=$objPost->dangchokiemduyet($page);
		}
		
		include 'Views/user/dangchokiemduyet.php';
	}
	public function dakiemduyet()
	{
		if (!empty($_GET['page'])) {
			$page=$_GET['page'];
		}else{
			$page=1;
		}
		SESSION::start();
		$objPost=new Post;
		if (!empty($_GET['key'])) {
			$post=$objPost->timkiembaidang($_GET['key'],$page);
		}
		else{
		$post=$objPost->dakiemduyet($page);
		}
		include 'Views/user/dakiemduyet.php';
	}
	public function daxoa()
	{
		if (!empty($_GET['page'])) {
				$page=$_GET['page'];
		}else{
				$page=1;
		}
		SESSION::start();
		$objPost=new Post;
		if (!empty($_GET['key'])) {
		$post=$objPost->timkiembaidang($_GET['key'],$page);
		}
		else{
		$post=$objPost->daxoa($page);
		}
		include 'Views/user/daxoa.php';
	}

	public function getstatuspost()
	{
		$objPost=new Post;
		
	}
}

?>
