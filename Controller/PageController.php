<?php 
/**
* 
*/
class PageController
{
	public function login()
	{
		session_start();
		if (!empty($_SESSION['id'])) {
			Helper::redirect(Helper::$siteurl);
		}
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$username=$_POST['username'];
			$pass=$_POST['pass'];
			$objUser=new User();
			if ($objUser->isUser($username,$pass)) {
				$user=$objUser->getUserByUsername($username);
				Session::start();
				Session::set('id',$user['id']);
				Session::set('username',$user['username']);
				Session::set('level',$user['level']);
				if (Helper::isAdmin()) {
					Helper::redirect(Helper::$siteurl."/user/dashboard");	
				}else{
					Helper::redirect(Helper::$siteurl);
				}

			}else{
				$login=false;
			}
		}
		include 'Views/pages/login.php';
	}
	public function contact()
	{
		
	}
	public function signup()
	{
		session_start();
		if (!empty($_SESSION['id'])) {
			Helper::redirect(Helper::$siteurl);
		}
		include 'Views/pages/signup.php';
	}
	public function signupsubmit()
	{
		$error=[];
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$username=$_POST["username"];			
			$pass=$_POST["pass"];			
			$pass2=$_POST["pass2"];			
			$email=$_POST["email"];
			if ($pass!=$pass2) {
				$error[]="passnotmatch";
			}
			$objUser=new User();

			if ($objUser->issetUser($username)>0) {
				$error[]='issetuser';
			}

			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$error[]="invalidemail";
			}

			if (count($error)>0) {
				include 'Views/pages/signup.php';
				//$this->signup();
			}else {
				$data=array(
					'username'=>$username,
					'password'=>md5($pass),
					'email'=>$email
					);
				//echo "Register Success";
				//Helper::redirect();
				if ($objUser->add($data)) {
					$just_reg=true;
					include 'views/pages/login.php';
				}else {
					echo "False";
				}
			}
		}
	}


	public function logout()
	{
		Session::start();
		Session::destroy();
		Helper::redirect(Helper::$siteurl);
	}
}
?>