<?php 
/**
* 
*/
class UserController
{
	public function update()
	{
		session_start();
		$objUser=new User;
		// cái dòng này không có form gửi sang
		// thì sao nó chạy\//
		// chỉ làm 1 lần nhá
		// lần sau cấm hỏi lại nhá :))
		// hỏi lại hôn 1 cái nhá ok?
		//mơ đi cưng vậy chấp nhận đừng hỏ lại :))
		//update thì cần ID và data
		//sql 
		// Update table SET a=x,b=y WHERE id=ID;
		//  
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			//dang dung dt vode hoi cham :))ngủ rồi à chưa thế sao dùng dt// để yên nào
			$data=array(
				'username'=>$_POST['username'],
				'email'=>$_POST['email'],
				'sodienthoai'=>$_POST['sodienthoai'],
				'level'=>$_POST['level']
				);	

			// chỗ này thiếu kiểm tra isset user, tạm thời chưa cần, để cho chặt chẽ thôi

			if ($objUser->update($_POST['id'],$data)) {
				$_SESSION['message']="Update thành công";
				$_SESSION['code']="success";
				//:)) update thành công và code bằng error, quá vui tính
				Helper::redirect(Helper::$siteurl."user/listuser");
			}

		}
	}
	public function updateprofile()
	{
		session_start();
		$objUser=new User;
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			//dang dung dt vode hoi cham :))ngủ rồi à chưa thế sao dùng dt// để yên nào
			$data=array(
				'fullname'=>$_POST['fullname'],
				'email'=>$_POST['email'],
				'sodienthoai'=>$_POST['sodienthoai']
				);	

			// chỗ này thiếu kiểm tra isset user, tạm thời chưa cần, để cho chặt chẽ thôi

			if ($objUser->update($_SESSION['id'],$data)) {
				$_SESSION['message']="Update thành công";
				$_SESSION['code']="success";
				//:)) update thành công và code bằng error, quá vui tính
				Helper::redirect(Helper::$siteurl."user/profile");
			}

		}
	}
	public function add()
	{


		$objUser=new User;
		/*if (!Helper::isAdmin()) {
			include 'Views/pages/permission.php';
			return;
		}*/
		session_start();
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$objUser=new User;
			if ($objUser->issetUser($_POST['username'])) {
				$_SESSION['message']='User exists';
				$_SESSION['code']='error';
				Helper::redirect(Helper::$siteurl."user/listuser");
				return;
			}

			$data=array(
				'username'=>$_POST['username'],
				'password'=>md5($_POST['password']),
				'email'=>$_POST['email'],
				'sodienthoai'=>$_POST['sodienthoai'],
				'level'=>$_POST['level']
				);
			if ($objUser->add($data)) {
				$_SESSION['message']='Add user success';
				$_SESSION['code']='success';
			}
			else{
				$_SESSION['message']='Add user false';
				$_SESSION['code']='error';
			}
			Helper::redirect(Helper::$siteurl."user/listuser");
		}
	}
	public function index()
	{
		SESSION::start();
		$objUser=new User;
		if (!$objUser->islogin()) {
			Helper::redirect(Helper::$siteurl."page/login");
		}
		$user=$objUser->getUserById($_SESSION['id']);
		include 'Views/user/index.php';
	}

	
	public function listpost()
	{
		SESSION::start();
		$active=1;
		$objUser=new User;
		if (!$objUser->islogin()) {
			Helper::redirect(Helper::$siteurl."page/login");
		}
		$objPost=new Post;
		$where="id_nguoiadd={$_SESSION['id']}";
		$listpost=$objPost->getlist($where);
		include 'Views/user/listpost.php';
	}
	
	public function changepass()
	{
		SESSION::start();
		$active=3;
		$objUser=new User;
		if (!$objUser->islogin()) {
			Helper::redirect(Helper::$siteurl."page/login");
		}
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$error=[];
			$old=$_POST['old'];
			$new=$_POST['new'];
			$new2=$_POST['new2'];
			if ($new!=$new2) {
				$error[]="passnotmatch";
			}

			if (!$objUser->isUser($_SESSION['username'],$old)) {
				$error[]="passnotright";
			}
			if (count($error)==0) {
				if ($objUser->updatepassword($_SESSION['id'],$new)) {
					$success=true;
				}
			}
		}
		include "Views/user/changepass.php";
	}
	
	public function listuser()
	{
		SESSION::start();
		$objUser=new User;
		/*if (!$objUser->islogin()) {
			include 'Views/pages/permission.php';
			return;
		}*/
		if (!empty($_GET['key'])) {
			$users=$objUser->timkiem($_GET['key']);

		}else{
			$users = $objUser->all();
		}
		include 'Views/user/index.php';

	}

	public function getdetail()
	{
		$objUser=new User;

		if (!empty($_GET['id'])) {
			$users=$objUser->getUser($_GET['id']);
			$id=$_GET['id'];
			$objPost=new Post;
			$post=$objPost->demsobaidang($id);
			$post2=$objPost->demSoBinhLuan($id);
			$where="id_nguoiadd=$id";
			$listpost=$objPost->getlist($where);
			$cmt="u_id=$id";
			$listcomment=$objPost->getComment($cmt);

		}
		include 'Views/user/userdetail.php';
	}
	
	public function dashboard()
	{
		$objUser=new User;
		$objPost=new Post;
		$objComment=new Comment;
		$tongbaidang=$objPost->tongbaidang();
		$tongbaidangdaduyet=$objPost->tongbaidangdaduyet();
		$tongnguoidung=$objUser->tongnguoidung();
		$tongbinhluan=$objComment->tongbinhluan();
		include 'Views/user/dashboard.php';
	}

	public function search()
	{
		$objUser=new User;
		if (!empty($_GET['key'])) {
			$key=$_GET['key'];
			if ($objUser->issetUserID($id)) {
				$users=$objUser->get();
				include 'Views/user/index.php';
			}
		}
	}
	public function deletepost()
	{
		session_start();
		$id=$_GET['id'];
		$objPost=new Post;
		$post=$objPost->getPost($id);

		if (!$objPost->issetPost($id)) {
			$_SESSION['code']='error';
			$_SESSION['message']="Không có id {$id} trong csdl";
			Helper::redirect(Helper::$siteurl."user/listpost");
		}else {
			if ($objPost->delete($id)) {
				Helper::xoafile('uploads/'.$post['img_folder']);
				$_SESSION['code']='success';
				$_SESSION['message']="Xóa thành công bài đăng {$post['tieude']} trong csdl";
				Helper::redirect(Helper::$siteurl."user/listpost");
			}
		}
	}
	public function profile()
	{
		session_start();
		$active=2;
		$objUser=new User;
		if (!$user=$objUser->issetUserID($_SESSION['id'])) {
			session_destroy();
			Helper::rediect(Helper::$siteurl."page/login");
		}else {
			$user=$objUser->getUserById($_SESSION['id']);
		}
		include 'Views/user/profile.php';
	}
	public function delete()
	{
		/*SESSION::start();
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}*/

		
		$objUser=new User;
		
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objUser->issetUserID($id)) {
				// kiểm tra xem có người dùng có id này không ?
				//nếu có thì lại dùng tiếp hàm delete trong user nó lấy id
				if ($objUser->delete($id)) {
					session_start();
					$_SESSION['message']='Xóa thành công';
					$_SESSION['code']='success';
					Helper::redirect(Helper::$siteurl."user/listuser");

				}
			}else {
				include 'Views/pages/404.php';
			}
		}else {
			include 'Views/pages/404.php';
		}
	}
}
?>