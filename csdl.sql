-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2017 at 10:59 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `timkiemtro`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `body`, `datetime`, `u_id`, `p_id`) VALUES
(2, 'Good job\r\n', '2017-03-26 14:36:37', 6, 7),
(3, 'Test Comment\r\n', '2017-03-26 14:36:47', 6, 7),
(4, 'wow', '2017-03-26 14:50:33', 6, 8),
(5, 'Test Comment 2\r\n', '2017-03-26 15:12:30', 6, 9),
(6, 'Nhà đẹp\r\n', '2017-03-26 15:12:52', 6, 8),
(7, 'Nhà xinh\r\n', '2017-03-26 15:13:16', 6, 10),
(8, 'Ac qua di mat', '2017-03-27 09:08:49', 6, 5),
(9, 'hand', '2017-04-21 13:32:43', 6, 7),
(10, 'oo\r\n', '2017-04-21 13:32:51', 6, 7),
(11, 'hey', '2017-04-21 13:44:19', 6, 11),
(12, 'i know it break your heart, move to the city in a broke down car and 4 year no call', '2017-04-21 13:45:29', 6, 11);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `tieude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gia` bigint(20) DEFAULT NULL,
  `dientich` float DEFAULT NULL,
  `mota` text COLLATE utf8_unicode_ci,
  `sodienthoai` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tinh_id` int(11) DEFAULT NULL,
  `huyen_id` int(11) DEFAULT NULL,
  `diachi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_folder` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_nguoiadd` int(11) NOT NULL,
  `add_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trangthai` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `tieude`, `gia`, `dientich`, `mota`, `sodienthoai`, `tinh_id`, `huyen_id`, `diachi`, `img_folder`, `id_nguoiadd`, `add_date`, `trangthai`) VALUES
(5, 'Căn hộ chung cư cao cấp', 8000000, 40, '<b>THÔNG TIN MÔ TẢ</b><br>Chính chủ cho thuê căn hộ cao cấp Nguyễn Khánh Toàn&nbsp;*RICH LAND*, mới 100%, 1PK, 1PN chuẩn khách sạn 3 sao. Đầy đủ tiện nghi chỉ việc sách valy đến ở. Tại số 23 ngõ 58 đường Nguyễn Khánh Toàn, Cầu Giấy, Hà Nội.[Khách có thể thuê căn hộ theo ngày, tuần, tháng, hoặc thuê lâu dài. Phù hợp người nước ngoài, Việt Nam, đi làm, công tác, du lịch, học tập]1.', ' 0974865500', 3, 29, 'Nguyễn Khánh Toàn, Quận Cầu Giấy, Hà Nội', '07e99f116b916656b3fd4f7dd58a3cbe', 6, '2017-01-31 01:56:26', 1),
(7, 'Nhà chính chủ 282 Khâm Thiên', 3000000, 20, 'Cho thuê một phòng tầng 4 trong căn nhà 5 tầng. Nhà huớng Đông Nam, cách mặt đuờng 8m, gần bến xe buýt. Có chỗ để xe, giuờng, tủ, điều hoà, nóng lạnh, wifi, có dây cáp mạng, toilet riêng. Giá điện nuớc tính theo giá nhà nước.', '0916497370', 3, 25, '282 Khâm Thiên', '022823b8aa03cffdb1189da300c18b25', 6, '2017-01-31 02:08:17', 1),
(8, 'CCMN siêu sang phố Nguyễn Đình Hoàn, Cầu Giấy', 4000000, 25, '- Nội thất sang trọng, thiết kế bắt mắt. Bao gồm: Tủ bếp, tủ quần áo, giường, nóng lạnh, điều hòa, quạt trần. Nhà mới đưa vào sửa dụng từ tháng 1/2017.<br>- An ninh trật tự rất tốt, nhà cao ráo, sạch sẽ và cực kỳ thoáng mát.Có ban công riêng cho từng căn hộ.<br><br>- Đèn led tiết kiệm điện<br><br>- Không gian yên tĩnh và văn minh.<br><br>- Riêng biệt, tự do, rất phù hợp với các gia đình trẻ.<br><br>- Thang máy rộng rãi, tốc độ cao.<br><br>- Thiết kế như chung cư cao cấp.<br><br>- Nhân viên bảo vệ trực 24/24. Có hệ thống camera an ninh, phòng cháy chữa cháy.<br><br>- Căn hộ khép kín rộng 22 m2, giá 3.8 tr/tháng. Có 4 loại phòng để quý khách lựa chọn(Luxury 1,2,3,4).&nbsp;<br><br>- Không thu phí thang máy, vệ sinh.<br><br>- Nhà mới xây – nội thất đẹp, hiện đại và tiện nghi, thoáng mát.<br><br>- Toilet ốp lát đẹp, lavabo, sen vòi, kệ gương… nóng lạnh lịch sự và đồng bộ.<br><br>- Có tủ bếp đồng bộ, sang trọng gồm tủ bếp, mặt đá, chậu rửa Inox, ….<br><br>- Internet cáp quang tốc độ cao. Mỗi căn hộ được trang bị một wifi riêng.<br><br>- Điện, nước công tơ riêng. Giá điện ưu đãi chỉ 3.400 đ/1kw.<br><br>- Liên hệ chị Lan A. Cường', '0913506880', 3, 29, 'Địa chỉ: Ngõ 142 Nguyễn Đình Hoàn - Cầu Giấy - Hà Nội.', '6246bbd28be0b22ff440cc5413329f36', 6, '2017-01-31 23:44:29', 1),
(9, 'Căn hộ Oficetel sẵn nội thất', 6000000, 55, 'Công ty chúng tôi đang có nhu cầu cho thuê các căn chung cư dạng officetel đa năng, phù hợp cho đa mục đích làm việc,học tập và sinh hoạt. Toà nhà toạ lạc tại vị trí đắc địa, dân cư văn minh, hai tầng để xe và thang máy siêu tốc. Ưu đãi lớn cho các khách hàng ký hợp đồng sớm và lâu dài với chúng tôi!', '0904371669', 3, 29, '', 'ab00b14a2da2e3cdcc44f06265db6574', 6, '2017-02-01 11:40:43', 1),
(10, 'Chung Cư Xuân Mai Tower gần siêu thị Metro', 3000000, 120, 'Cần cho thuê chung cư gần Metro Hà Đông Khu B, căn góc hướng về phía bể bơi view về trung tâm thành phố như vị trí đóng khung trong hình mặt bằng, diện tích 120 m2,tầng 10, điều hòa nóng lạnh đủ cả, sàn gỗ, ba phòng ngủ 2 nhà vệ sinh, giá cả thương lượng', '0123456789', 3, 29, 'gần siêu thị Metro Hà Đông', 'b8599b9343f82e3bc30984ca4f3fea3f', 6, '2017-02-01 11:43:35', 1),
(11, 'CHCC cao cấp KDT Mỹ Đình', 16000000, 95, 'Tên: Quangcuong\r\nE-mail: cuong.bacland@gmail.com\r\nSố điện thoại: \r\nChuyên mục: Căn hộ/Chung cư\r\nBạn đăng tin: Cho thuê\r\nVùng: Hà Nội\r\nTỉnh, thành, quận: Quận Bắc Từ Liêm\r\nTựa đề: Chủ nhà cần cho thuê CHCC cao cấp KĐT Mỹ Đình\r\nSố phòng: 3\r\nDiện tích: 95 m²\r\nĐịa chỉ: Chung cư CT5 ĐN2 Nguyễn Cơ Thạch, Mỹ Đình 2\r\nTình trạng: Đã sử dụng\r\nNội dung tin: Chính chủ cần cho thuê nhanh căn hộ chung cư cao cấp tại tòa CT5 Đơn nguyên 2, Đường Nguyễn Cơ Thạch, Mỹ đình 2.\r\nCăn hộ có diện tích 95m2, thiết kế cực kì hợp lí, nội thất trang bị cao cấp, đầy đủ cho người nước ngoài thuê chỉ việc xách vali đến ở. Hướng thoáng mat, tất cả các phòng ngủ đều có anh sáng.\r\nGiá thuê 16,5tr. Liên hệ: \r\nGiá: 16 500 000 đ/tháng', '01234567989', 3, 29, '93, Hoang Mai', 'ca87e9d08238e6d61791dc55931bb500', 6, '2017-02-01 11:46:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `sodienthoai` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `sodienthoai`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801f', 'zangquyet@gmail.com', NULL, 0),
(2, 'hand', '573ce5969e9884d49d4fab77b09a30', 'zangquyet@gmail.com', NULL, 0),
(3, 'kienmui', '7335c333f060e51239d9a8e04d59ca', 'zangquyet@gmail.com', NULL, 0),
(4, 'user', 'ee11cbb19052e40b07aac0ca060c23', 'sadhvsgd@gmail.com', NULL, 0),
(6, 'kienmui1', '21232f297a57a5a743894a0e4a801fc3', 'kienmui@gmail.com', NULL, 0),
(7, 'bocon', 'b0aac28c82d24f491c08cb40e42d8ffa', 'zangquyet@gmail.com', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `u_id` (`u_id`),
  ADD KEY `p_id` (`p_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`p_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
